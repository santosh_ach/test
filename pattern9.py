import sys

# Now ask for input
num = input("Enter number of * to print: ")
if not num.isdigit():
    print("Invalid input. Kindly enter number only.")
    sys.exit()

print("You have entered: ", int(num))

for row in range (int(num) + 1):
    for col in range (int(num) + 1 - row):
        sys.stdout.write(' *')
    for col in range (int(num) + 1 + row):
        for row in range (col):
            sys.stdout.write(' ')
    print('')
